\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {subsection}{\numberline {1.1}Tone Discrimination}{2}
\contentsline {subsection}{\numberline {1.2}Pitch matching while singing}{3}
\contentsline {subsection}{\numberline {1.3}Requirements}{3}
\contentsline {subsubsection}{\numberline {1.3.1}Tone Discrimination}{3}
\contentsline {subsection}{\numberline {1.4}Repositories that can be used}{3}
\contentsline {subsubsection}{\numberline {1.4.1}Web audio API}{3}
\contentsline {subsubsection}{\numberline {1.4.2}Plotting pitch data gathered from web audio API}{3}
\contentsline {subsubsection}{\numberline {1.4.3}Measure to evaluate correctness of the imitation}{4}
\contentsline {subsection}{\numberline {1.5}Typography of melograph signals}{4}
